import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Astuces from "@/views/Astuces";
import Pity from "@/views/Pity";
import ManoirTaishan from "@/views/donjons/ManoirTaishan";
import GorgeDeLOubli from "@/views/donjons/GorgeDeLOubli";
import ResinePlanner from "@/views/ResinePlanner";
import Abysses from "@/views/Abysses";
import PepiniereDeCecilias from "@/views/donjons/PepiniereDeCecilias";
import PalaisSecretDeLianshan from "@/views/donjons/PalaisSecretDeLianshan";
import BuildPerso from "@/views/BuildPerso";
import Donate from "@/views/Donate";
import ResMobs from "@/views/ResMobs";
import CalculPity from "@/views/CalculPity";
import Artefacts from "@/views/Artefacts";

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/resine-planner',
    name: 'ResinePlanner',
    component: ResinePlanner
  },
  {
    path: '/astuces',
    name: 'Astuces',
    component: Astuces
  },
  {
    path: '/pity',
    name: 'Pity',
    component: Pity
  },
  {
    path: '/artefacts',
    name: 'Artefacts',
    component: Artefacts
  },
  {
    path: '/resistances-monstres',
    name: 'ResMobs',
    component: ResMobs
  },
  {
    path: '/abysses',
    name: 'Abysses',
    component: Abysses
  },
  {
    path: '/abysses/:pageNum',
    name: 'AbyssesB',
    component: Abysses
  },
  {
    path: '/build/:name',
    name: 'BuildPerso',
    component: BuildPerso
  },
  {
    path: '/donjon/manoir-taishan',
    name: 'ManoirTaishan',
    component: ManoirTaishan
  },
  {
    path: '/donjon/gorge-de-l-oubli',
    name: 'GorgeDeLOubli',
    component: GorgeDeLOubli
  },
  {
    path: '/donjon/pepiniere-de-cecilias',
    name: 'PepiniereDeCecilias',
    component: PepiniereDeCecilias
  },
  {
    path: '/donjon/palais-secret-de-lianshan',
    name: 'PalaisSecretDeLianshan',
    component: PalaisSecretDeLianshan
  },
  {
    path: '/donate',
    name: 'Donate',
    component: Donate
  },
  {
    path: '/calcul-pity',
    name: 'CalculPity',
    component: CalculPity
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  }
]

const router = new VueRouter({
  routes
})

export default router
