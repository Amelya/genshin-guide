import '@babel/polyfill'
import 'mutationobserver-shim'
import Vue from 'vue'
import './plugins/bootstrap-vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import '@mdi/font/css/materialdesignicons.css'
import * as dayjs from 'dayjs';
import 'dayjs/locale/fr'
import "./plugins/firebase";
import VCalendar from 'v-calendar';

Vue.use(VCalendar);

dayjs.locale('fr');

Vue.config.productionTip = false

new Vue({
    router,
    render: h => h(App)
}).$mount('#app')
