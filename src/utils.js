import dayjs from 'dayjs'

//Renvoie le bon tableau en fonction du jour actuel
export function detectDayOfWeek(lundi_jeudi, mardi_vendredi, mercredi_samedi, dimanche) {
    let cards
    let dayOfWeek = dayjs().format('dddd')

    switch (dayOfWeek) {
        case "lundi":
            cards = lundi_jeudi
            break;
        case "mardi":
            cards = mardi_vendredi
            break;
        case "mercredi":
            cards = mercredi_samedi
            break;
        case "jeudi":
            cards = lundi_jeudi
            break;
        case "vendredi":
            cards = mardi_vendredi
            break;
        case "samedi":
            cards = mercredi_samedi
            break;
        case "dimanche":
            cards = dimanche
            break;
        default:
            cards = []
            break;
    }

    return cards;
}

//Trie le JSON en fonction du Nom
export function orderByName(array) {
    array.sort(function (a, b) {
        return a.name.localeCompare(b.name);
    });
}

//Trie le JSON en fonction du Donjon
export function orderByDonjon(array) {
    array.sort(function (a, b) {
        return a.donjon.localeCompare(b.donjon);
    });
}

//Trie le JSON en fonction des matériaux d'armes
export function orderByMat(array) {
    array.sort(function (a, b) {
        return a.imageMat.localeCompare(b.imageMat);
    });
}

//Trie le JSON en fonction des matériaux d'aptitudes
export function orderByApt(array) {
    array.sort(function (a, b) {
        return a.imageApt.localeCompare(b.imageApt);
    });
}

//Trie le JSON en fonction de la rareté du perso
export function orderByStars(array) {
    array.sort(function (a, b) {
        return a.star.localeCompare(b.star);
    });
}

//Événements du calendrier
export const events = [
    {
        key: 'today',
        highlight: true,
        dates: new Date()
    },
    {
        dot: 'red',
        popover: {
            label: "1.6",
            visibility: 'focus'
        },
        dates: [
            new Date('2021-06-09')
        ],
    },
]

export const resineOptions = [
    {value: null, text: 'Veuillez choisir une option'},
    {value: {name: 'Andros [le loup du nord]', value: 60, image: 'andros'}, text: 'Andros [le loup du nord] (60)'},
    {
        value: {name: 'Dvalin [le dragon Stormterror]', value: 60, image: 'dvalin'},
        text: 'Dvalin [le dragon Stormterror] (60)'
    },
    {value: {name: 'Tartaglia', value: 60, image: 'childe'}, text: 'Tartaglia (60)'},
    {value: {name: 'Arbre enflammé', value: 40, image: 'arbre_pyro'}, text: 'Arbre enflammé (40)'},
    {value: {name: 'Arbre congelé', value: 40, image: 'arbre_cryo'}, text: 'Arbre congelé (40)'},
    {value: {name: 'Océanide', value: 40, image: 'oceanide'}, text: 'Océanide (40)'},
    {value: {name: 'Hypostase Électro', value: 40, image: 'hypostase_electro'}, text: 'Hypostase Électro (40)'},
    {value: {name: 'Hypostase Anémo', value: 40, image: 'hypostase_anemo'}, text: 'Hypostase Anémo (40)'},
    {value: {name: 'Hypostase Géo', value: 40, image: 'hypostase_geo'}, text: 'Hypostase Géo (40)'},
    {
        value: {name: 'Donjon [Aptitudes] Condensé', value: 40, image: 'donjon_aptitudes'},
        text: 'Donjon [Aptitudes] Condensé (40)'
    },
    {value: {name: 'Donjon [Aptitudes]', value: 20, image: 'donjon_aptitudes'}, text: 'Donjon [Aptitudes] (20)'},
    {value: {name: 'Donjon [Armes] Condensé', value: 40, image: 'donjon_armes'}, text: 'Donjon [Armes] Condensé (40)'},
    {value: {name: 'Donjon [Armes]', value: 20, image: 'donjon_armes'}, text: 'Donjon [Armes] (20)'},
    {
        value: {name: 'Donjon [Artéfacts] Condensé', value: 40, image: 'donjon_artefacts'},
        text: 'Donjon [Artéfacts] Condensé (40)'
    },
    {value: {name: 'Donjon [Artéfacts]', value: 20, image: 'donjon_artefacts'}, text: 'Donjon [Artéfacts] (20)'},
    {
        value: {name: 'Ligne énergétique [EXP] Condensé ', value: 40, image: 'ligne_bleu'},
        text: 'Ligne énergétique [EXP] Condensé (40)'
    },
    {value: {name: 'Ligne énergétique [EXP]', value: 20, image: 'ligne_bleu'}, text: 'Ligne énergétique [EXP] (20)'},
    {
        value: {name: 'Ligne énergétique [Moras] Condensé ', value: 40, image: 'ligne_jaune'},
        text: 'Ligne énergétique [Moras] Condensé (40)'
    },
    {
        value: {name: 'Ligne énergétique [Moras]', value: 20, image: 'ligne_jaune'},
        text: 'Ligne énergétique [Moras] (20)'
    },
    {
        value: {name: 'Résine Magique [Forgeron]', value: 10, image: 'magical_crystal'},
        text: 'Résine Magique [Forgeron] (10)'
    },
]

export const NavBuild4Stars = [
    {
        name: 'Amber',
        image: 'amber.png',
        disabled: false
    },
    {
        name: 'Barbara',
        image: 'barbara.png',
        disabled: false
    },
    {
        name: 'Beidou',
        image: 'beidou.png',
        disabled: false
    },
    {
        name: 'Bennett',
        image: 'bennett.png',
        disabled: false
    },
    {
        name: 'Chongyun',
        image: 'chongyun.png',
        disabled: false
    },
    {
        name: 'Diona',
        image: 'diona.png',
        disabled: false
    },
    {
        name: 'Fischl',
        image: 'fischl.png',
        disabled: false
    },
    {
        name: 'Kaeya',
        image: 'kaeya.png',
        disabled: false
    },
    {
        name: 'Lisa',
        image: 'lisa.png',
        disabled: false
    },
    {
        name: 'Ningguang',
        image: 'ningguang.png',
        disabled: false
    },
    {
        name: 'Noelle',
        image: 'noelle.png',
        disabled: false
    },
    {
        name: 'Razor',
        image: 'razor.png',
        disabled: false
    },
    {
        name: 'Rosalia',
        image: 'rosalia.png',
        disabled: false
    },
    {
        name: 'Sucrose',
        image: 'sucrose.png',
        disabled: false
    },
    {
        name: 'Xiangling',
        image: 'xiangling.png',
        disabled: false
    },
    {
        name: 'Xingqiu',
        image: 'xingqiu.png',
        disabled: false
    },
    {
        name: 'Xinyan',
        image: 'xinyan.png',
        disabled: false
    },
    {
        name: 'Yanfei',
        image: 'yanfei.png',
        disabled: false
    },
    {
        name: 'Sayu',
        image: 'sayu.png',
        disabled: true
    },
]

export const NavBuild5Stars = [
    {
        name: 'Albedo',
        image: 'albedo.png',
        disabled: false
    },
    {
        name: 'Diluc',
        image: 'diluc.png',
        disabled: false
    },
    {
        name: 'Eula',
        image: 'eula.png',
        disabled: false
    },
    {
        name: 'Ganyu',
        image: 'ganyu.png',
        disabled: false
    },
    {
        name: 'Hu tao',
        image: 'hutao.png',
        disabled: false
    },
    {
        name: 'Jean',
        image: 'jean.png',
        disabled: false
    },
    {
        name: 'Keqing',
        image: 'keqing.png',
        disabled: false
    },
    {
        name: 'Klee',
        image: 'klee.png',
        disabled: false
    },
    {
        name: 'Mona',
        image: 'mona.png',
        disabled: false
    },
    {
        name: 'Qiqi',
        image: 'qiqi.png',
        disabled: false
    },
    {
        name: 'Tartaglia',
        image: 'tartaglia.png',
        disabled: false
    },
    {
        name: 'Venti',
        image: 'venti.png',
        disabled: false
    },
    {
        name: 'Voyageur',
        image: 'lumine.png',
        disabled: false
    },
    {
        name: 'Xiao',
        image: 'xiao.png',
        disabled: false
    },
    {
        name: 'Zhongli',
        image: 'zhongli.png',
        disabled: false
    },
    {
        name: 'Kazuha',
        image: 'kazuha.png',
        disabled: true
    },
    {
        name: 'Ayaka',
        image: 'ayaka.png',
        disabled: true
    },
    {
        name: 'Yoimiya',
        image: 'yoimiya.png',
        disabled: true
    },
]

export const tableResMobs = [
    {
        imageName: 'cryo_slime.png',
        resElectro: '5',
        resPyro: '0',
        resHydro: '10',
        resCryo: '100',
        resAnemo: '10',
        resGeo: '10',
        resPhysique: '10',
        immunity: 'cryo',
        weakness: 'pyro'
    },
    {
        imageName: 'pyro_slime.png',
        resElectro: '5',
        resPyro: '100',
        resHydro: '0',
        resCryo: '5',
        resAnemo: '10',
        resGeo: '10',
        resPhysique: '10',
        immunity: 'pyro',
        weakness: 'hydro'
    },
    {
        imageName: 'hydro_slime.png',
        resElectro: '0',
        resPyro: '10',
        resHydro: '100',
        resCryo: '0',
        resAnemo: '10',
        resGeo: '10',
        resPhysique: '10',
        immunity: 'hydro',
        weakness: 'electro / cryo'
    },
    {
        imageName: 'electro_slime.png',
        resElectro: '100',
        resPyro: '5',
        resHydro: '5',
        resCryo: '5',
        resAnemo: '10',
        resGeo: '10',
        resPhysique: '10',
        immunity: 'electro',
        weakness: 'hydro / cryo / pyro'
    },
    {
        imageName: 'anemo_slime.png',
        resElectro: '10',
        resPyro: '10',
        resHydro: '10',
        resCryo: '10',
        resAnemo: '100',
        resGeo: '10',
        resPhysique: '10',
        immunity: 'anemo',
        weakness: '-'
    },
    {
        imageName: 'geo_slime.png',
        resElectro: '10',
        resPyro: '10',
        resHydro: '10',
        resCryo: '10',
        resAnemo: '10',
        resGeo: '100',
        resPhysique: '10',
        immunity: 'geo (sauf pour bouclier)',
        weakness: '-'
    },
    {
        imageName: 'dendro_slime.png',
        resElectro: '10',
        resPyro: '0',
        resHydro: '10',
        resCryo: '10',
        resAnemo: '10',
        resGeo: '10',
        resPhysique: '10',
        immunity: '-',
        weakness: 'pyro'
    },
    {
        imageName: 'arbre_pyro.png',
        resElectro: '10',
        resPyro: '100',
        resHydro: '0',
        resCryo: '10',
        resAnemo: '10',
        resGeo: '10',
        resPhysique: '10',
        immunity: 'pyro',
        weakness: 'hydro'
    },
    {
        imageName: 'arbre_cryo.png',
        resElectro: '5',
        resPyro: '0',
        resHydro: '10',
        resCryo: '100',
        resAnemo: '10',
        resGeo: '10',
        resPhysique: '10',
        immunity: 'cryo',
        weakness: 'pyro'
    },
    {
        imageName: 'hypostase_electro.png',
        resElectro: '100',
        resPyro: '5',
        resHydro: '10',
        resCryo: '5',
        resAnemo: '10',
        resGeo: '10',
        resPhysique: '10',
        immunity: 'electro',
        weakness: 'pyro / cryo'
    },
    {
        imageName: 'hypostase_anemo.png',
        resElectro: '10',
        resPyro: '10',
        resHydro: '10',
        resCryo: '10',
        resAnemo: '100',
        resGeo: '10',
        resPhysique: '10',
        immunity: 'anemo',
        weakness: '-'
    },
    {
        imageName: 'hypostase_geo.png',
        resElectro: '10',
        resPyro: '10',
        resHydro: '10',
        resCryo: '10',
        resAnemo: '10',
        resGeo: '100',
        resPhysique: '10',
        immunity: 'geo (sauf pilier)',
        weakness: 'claymore'
    },
    {
        imageName: 'andros.png',
        resElectro: '7',
        resPyro: '7',
        resHydro: '10',
        resCryo: '100',
        resAnemo: '100',
        resGeo: '10',
        resPhysique: '10',
        immunity: 'anemo / cryo',
        weakness: 'pyro / electro'
    },
    {
        imageName: 'dvalin.png',
        resElectro: '10',
        resPyro: '10',
        resHydro: '10',
        resCryo: '10',
        resAnemo: '10',
        resGeo: '10',
        resPhysique: '10',
        immunity: '-',
        weakness: '-'
    },
    {
        imageName: 'childe.png',
        resElectro: '30',
        resPyro: '10',
        resHydro: '30',
        resCryo: '10',
        resAnemo: '10',
        resGeo: '10',
        resPhysique: '10',
        immunity: '-',
        weakness: ' electro (phase 1) / hydro (phase 2)'
    }
]
