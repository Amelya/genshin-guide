module.exports = {
    productionSourceMap: false,
    "publicPath": "./",
    pwa: {
        name: 'Genshin Impact Guide',
        themeColor: '#FFFFFF',
        msTileColor: '#3C60FF',
        workboxOptions: {
            skipWaiting: true,
            clientsClaim: true,
        }
    },
    css: {
        loaderOptions: {
            sass: {
                additionalData: "@import '~@/assets/css/Main.scss'"
            },
        }
    }
}
